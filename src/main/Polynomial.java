
/** Integer-only polynomials. */
public class Polynomial {

    /** Create new instance with given coefficients.
     *
     * x^2 + 3x - 7 would be created by new Polynomial(-7, 3, 1)
     * x^5 + 42 would be created by new Polynomial(42, 0, 0, 0, 0, 1)
     *
     * @param coef Coefficients, ordered from lowest degree (power).
     */
    public Polynomial(int... coef) {
        System.out.printf("Creating polynomial");
        for (int i = 0; i < coef.length; i++) {
            if (i > 0) {
                System.out.print(" +");
            }
            System.out.printf(" %d * x^%d", coef[i], i);
        }
        System.out.println(" ...");
    }

    /** Get coefficient value for given degree (power). */
    public int getCoefficient(int deg) {
        return 0;
    }

    /** Get degree (max used power) of the polynomial. */
    public int getDegree() {
        return 0;
    }

    /** Format into human-readable form with given variable. */
    public String toPrettyString(String variable) {
        return "0";
    }

    /** Debugging output, dump only coefficients. */
    @Override
    public String toString() {
        return "Polynomial[0]";
    }

    /** Adds together given polynomials, returning new one. */
    public static Polynomial sum(Polynomial... polynomials) {
        return new Polynomial(0);
    }

    /** Multiplies together given polynomials, returning new one. */
    public static Polynomial product(Polynomial... polynomials) {
        return new Polynomial(0);
    }

    /** Get the result of division of two polynomials, ignoring remaineder. */
    public static Polynomial div(Polynomial dividend, Polynomial divisor) {
        return new Polynomial(0);
    }

    /** Get the remainder of division of two polynomials. */
    public static Polynomial mod(Polynomial dividend, Polynomial divisor) {
        return new Polynomial(0);
    }
}
